1 - Ease: Because a GUI is much more visually intuitive, users typically pick up on how to use a GUI faster than a command line interface.
2 -Control:  A GUI offers a lot of access to files, software features, and the operating system as a whole. Being more user friendly than a command line, especially for new or novice users, a GUI is utilized by more users.
3 - Multitasking: GUI users have windows that enable a user to view, control, manipulate, and toggle through multiple programs and folders at same time.
4 - Scripting: Creating scripts using a GUI has become much easier with the help of programming software, which allows users to write the scripts without having to know all the commands and syntax. Programming software provides guides and tips for how to code specific functions, as well as preview options to see if and how the script will work.
5 - Remote Access: Remotely access another computer or server is possible in a GUI and easy to navigate with little experience. IT professionals typically use a GUI for remote access, including the management of servers and user computers.
6 - Strain: The use of shortcut keys and more frequent movement of hand positions, due to switching between a keyboard and a mouse, strain may be reduced. Visual strain can still be a risk, but a GUI has more colors and is more visually appealing, leading to a potential reduction in visual strain.

References:
https://www.computerhope.com/issues/ch000619.htm
https://blogs.technet.microsoft.com/mscom/2007/03/26/the-gui-versus-the-command-line-which-is-better-part-2/